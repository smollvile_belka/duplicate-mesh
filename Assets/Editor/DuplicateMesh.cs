﻿using UnityEditor;
using UnityEngine;

public class DuplicateMesh : EditorWindow
{

    private static Mesh mesh;

    private static string folderPath = "Assets";
    private string nameFolder;
    private string nameMesh;

    [MenuItem("Tools/DuplicateMesh")]
    public static void duplicateMesh()
    {
        foreach(Object obj in Selection.objects)
        {
            if(obj is Mesh) 
                mesh = obj as Mesh;
        }

        EditorWindow.GetWindow(typeof(DuplicateMesh));
    }

    void OnGUI () 
    {
        
        if(GUI.Button(new Rect(130, 10, 50, 20), "Folder"))
            folderPath = EditorUtility.OpenFolderPanel("Select a folder", folderPath, "");

        if(folderPath != null)
        {
            EditorGUI.HelpBox(new Rect(10, 35, position.width - 20, 40), folderPath, MessageType.Info);

            if(folderPath.IndexOf("Asset") != 0)
                folderPath = folderPath.Substring(folderPath.IndexOf("Asset"));
        }

        nameFolder = EditorGUI.TextField(new Rect(10, 85, position.width - 20, 20),
            "Name Folder",
            nameFolder);

        nameMesh = EditorGUI.TextField(new Rect(10, 110, position.width - 20, 20),
            "Name Mesh",
            nameMesh);

        if(GUI.Button(new Rect(130, 140, 70, 40), "Duplicate"))
        {

            if(mesh != null)
            
                if(nameMesh != null)
                {
                    if(nameFolder != null)
                        if(!AssetDatabase.IsValidFolder(folderPath + "/" + nameFolder))
                            AssetDatabase.CreateFolder(folderPath, nameFolder);

                    saveCopyMesh();
                }
                else Debug.Log("The Mesh title is required!");

            else Debug.Log("Select Mesh");

            this.Close();
        }
    }

    public void saveCopyMesh()
    {
        Mesh secondaryMesh = new Mesh();

        secondaryMesh.name = nameMesh;
        secondaryMesh.vertices = mesh.vertices;
        secondaryMesh.triangles = mesh.triangles;
        secondaryMesh.normals = mesh.normals;
        secondaryMesh.uv = mesh.uv;

        AssetDatabase.CreateAsset(secondaryMesh, folderPath + "/" + nameFolder + "/" + secondaryMesh.name + ".asset");
    }
}
